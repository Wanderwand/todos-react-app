const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],

  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      flexGrow:{
        '2' : 2,
        '3' :3,
        '4' : 4,

      },
      colors: {
        primary: colors.indigo[400]
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
