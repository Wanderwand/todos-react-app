import React, { useEffect, useState } from "react";
import Input from "./elements/Input";
import TodoCard from "./elements/TodoCard";
import { Button, AddButton } from "./elements/Button";
import "./todo.css";
import axios from "axios"
import envConfig from "./../config/env.config";

const todoTemp = {
  name: "Task ",
  completed: false,
  dateCompletion:  `${new Date()}`,
};

function Todo() {
  const [todos, setTodos] = useState([]);
  const [bufferTask, setBufferTask] = useState({ ...todoTemp});
  
  const changeTaskValue = e=>{
    const buffer = bufferTask ;
    buffer.name = e.target.value ;
    setBufferTask({...buffer})
  } 
  const changeCompletionDate = e=>{
    const buffer = bufferTask;
    buffer.dateCompletion = e.target.value
    setBufferTask({...buffer})
  }
  const changeStatus = (e,i)=>{
    const arr = todos ;
    arr[i].completed = !arr[i].completed
    setTodos([...arr])
    axios.put(`${envConfig.apiUrl}/todo`,{id : todos[i].id , status : todos[i].completed})
    .then(res=>{
      // setTodos([...res.data])
      console.log("updated")
    })
    .catch(err=>console.log(err))
  }
  const saveTask = e=>{
    // console.log(bufferTask)
    axios.post(`${envConfig.apiUrl}/todo`,bufferTask)
    .then(res=>{
      setTodos([...todos,res.data])
    })
    .catch(err=>console.log(err))
  }


  const deleteTodo = (e, i) => {
    axios.delete(`${envConfig.apiUrl}/todo/`+todos[i].id )
    .then(res=>{
      const arr = todos;
      arr.splice(i, 1);
      setTodos([...arr]);
      console.log("updated")
    })
    .catch(err=>console.log(err))
  };
  

  useEffect(() => {
    if(todos.length===0){axios.get(`${envConfig.apiUrl}/todo`)
    .then(res=>{
      setTodos([...res.data])
    })
    .catch(err=>console.log(err))}
  }, [todos])

  return (
    <div>
     
      {/* new task details*/}
      <div className="newTask bg-indigo-100 p-8 lg:mx-20 mx-8">
        <Input type="text" placeholder="Add task" value={bufferTask.value} onChange ={changeTaskValue} />
        {/* CompleteTion date */}
        <div>
          <label for="dateCompletion">Complete Till : </label>
          <Input type="date" onChange={changeCompletionDate} ></Input>
        </div>
      </div>
      {/* save task button  */}
      <Button value ="Add Task" onClick={saveTask}></Button>

      <hr  className="" />
      <div className="tasks flex flex-col items-center m-5">
        <h3 className="text-gray-900 font-bold unerline mb-3"> ToDos</h3>
        {todos.map((todo,i)=>{
          return (
              <TodoCard todo= {todo} i={i} onChange ={e=>changeStatus(e,i)} onClick={e=>deleteTodo(e,i)}></TodoCard>
            )
        })}

      </div>  

      {/* <Button value ="Update Task" onClick={updateTask}></Button> */}

    

    </div>
  );
}

export default Todo;
