import React from "react";

export default function(props) {
  return (
    <input
      type={props.type}
      placeholder={props.placeholder}
      value={props.value}
      onChange = {props.onChange}
      className={`${props.type==="text" ? "w-3/4" : "" } `+ ` shadow border-b-2 focus:border-indigo-400 m-2 rounded p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
    
    />
  );
}

