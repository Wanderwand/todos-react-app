import React, { useEffect, useState } from "react";
import { AddButton } from "./Button";

function TodoCard(props) {
  
//   const createdDate = ()=>{
//     let createdDate = new Date(props.todo.dateCreated)
//     const todayDate = new Date();
//     let  result = todayDate - createdDate ; 
//     result = new Date(result)
//     return `${result.getHours()}:${result.getMinutes()} `
//   }

//   const completionDate = ()=>{
//     const date = new Date(props.todo.dateCompletion);
//     return date.toLocaleDateString()
//   }
 
const createdDate =()=>{
  const date = new Date(props.todo.dateCreated);
  return date.toLocaleString()
}
// completion date 
const completionDate = (todo)=>{
  const date = new Date(props.todo.dateCompletion);
  return date.toLocaleString()
}

  return (
    <div className={`m-2 p-2 w-3/4 md:flex shadow rounded ` + `${props.todo.completed ? "bg-gray-200" : ""}`}>

      <input type="checkbox" checked={props.todo.checked}  onChange={props.onChange} className="flex-grow-none mb-3 "></input>
   
      <div className={`flex flex-wrap flex-grow-3 px-4 ` + `${props.todo.completed ? "italic" : ""}` }>
        <div className="flex-grow-3 text-left">
          {props.todo.completed ? <div className= " line-through" >{props.todo.name}</div> : <div className= "flex-4 " >{props.todo.name}</div>}
          <div className="text-gray-500 "> {props.todo.completed ? "Completed" : "Incomplete"}</div>
        </div>

        <div className="text-left flex-grow-1">
          <div className="text-gray-700 "><b className="text-gray-700">Created : {createdDate()} </b></div>
          <div className="text-gray-700">Complete Till : <b>{completionDate()} </b></div>
        </ div>
      </div>
      <AddButton value="x" onClick={props.onClick}/>

    </div>
  );
}
export default TodoCard;
