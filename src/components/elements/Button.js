import React from "react";

export function Button(props) {
  return (
    <button
      onClick={props.onClick}
      className="border shadow-lg border-gray-700 bg-gray-700 text-white rounded-md px-4 py-2 my-5 transition duration-500 ease select-none hover:bg-gray-200 hover:text-gray-700 focus:outline-none focus:shadow-outline"
    >
      {props.value}
    </button>
  );
}

export function AddButton(props) {
  return (
    <button
      onClick={props.onClick}
      className=" rounded border-gray-400 text-gray-400  px-2 m-2 hover:text-white hover:bg-gray-300"
    >
      {props.value}
    </button>
  );
}
