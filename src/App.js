import "./App.css";
import Excalidraw from "./components/Excalidraw";
import Todo from "./components/Todo";
import { Button } from "./components/elements/Button";
import { useState } from "react";

function App() {
  const [isActive , setIsActive] = useState(false)
  const [value , setValue] = useState("Draw with Excalidraw")

  const onClick = e =>{
    if(isActive===false){
      setValue("Close Exaclidraw")
    }else{
      setValue("Draw with Excalidraw")
    }
    setIsActive(!isActive)
  }

  return (
    <div className="App items-center justify-center">
      <header className="App-header">Todos app</header>
      <Button value={value} onClick={onClick}></Button>
      {isActive ?  
      (<div className="bg-gray-200 mb-10">
        <Excalidraw />
        <hr></hr>
      </div>) : null
      }

      <Todo />
    </div>
  );
}

export default App;
